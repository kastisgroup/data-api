FROM openjdk:10.0.1-jre-slim

ARG jar_name=data-api-0.0.1-SNAPSHOT-jar-with-dependencies.jar

ADD target/${jar_name} .

CMD ["java", "-jar", "${jar_name}"]
