package com.kastisgroup.dataapi.controllers;

public class Endpoints
{
    public static final String GET_EQUITY_BY_SYMBOL = "/product/equities";
    public static final String GET_FUTURE_BY_SYMBOL = "/product/futures";
    public static final String GET_OPTION_BY_SYMBOL = "/product/options";
    public static final String GET_CROSS_REFERENCES = "/product/cross-references";
    public static final String GET_VENUES           = "/product/venues";

    public static final String GET_ORDER_BY_ID = "/order/id";

}
