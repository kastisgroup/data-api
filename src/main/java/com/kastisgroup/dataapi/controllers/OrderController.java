package com.kastisgroup.dataapi.controllers;

import com.kastisgroup.dataapi.messages.order.Order;
import com.kastisgroup.dataapi.services.OrderService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Optional;

@RestController
public class OrderController
{
    private OrderService orderService;

    public OrderController(OrderService orderService)
    {
        this.orderService = orderService;
    }

    /**
     * Retrieves an {@link Order} when given a client order ID
     *
     * @param clientOrderId An ID for an order as defined by the client
     * @param systemOrderId An ID for an order as defined by an upstream system
     * @return An {@link Order} matching the symbol or
     */
    @GetMapping(Endpoints.GET_ORDER_BY_ID)
    private Order getOrderById(@RequestParam("ClientOrderId") Optional<String> clientOrderId,
                               @RequestParam("SystemOrderId") Optional<String> systemOrderId)
    {
        if (clientOrderId.isPresent())
            return orderService.getOrderByClientOrderId(clientOrderId.get());
        else if (systemOrderId.isPresent())
            return orderService.getOrderBySystemOrderId(systemOrderId.get());
        return null;
    }

}
