package com.kastisgroup.dataapi.controllers;

import com.kastisgroup.dataapi.messages.common.components.CrossReferences;
import com.kastisgroup.dataapi.messages.common.components.Symbol;
import com.kastisgroup.dataapi.messages.common.components.Venue;
import com.kastisgroup.dataapi.messages.product.EquityProductInstrument;
import com.kastisgroup.dataapi.messages.product.FutureProductInstrument;
import com.kastisgroup.dataapi.messages.product.OptionProductInstrument;
import com.kastisgroup.dataapi.services.ProductService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

@RestController
public class ProductController
{
    private ProductService productService;

    public ProductController(ProductService productService)
    {
        this.productService = productService;
    }


    /**
     * Retrieves a {@link EquityProductInstrument} when given a symbol and an optional venue
     *
     * @param symbolProvider The source of this symbol
     * @param symbol         A code used by the provider to reference a specific futures contract
     * @param venue          An optional venue - some provider symbols are only unique per venue
     * @return A {@link EquityProductInstrument} matching the symbol or
     */
    @GetMapping(Endpoints.GET_EQUITY_BY_SYMBOL)
    private EquityProductInstrument getEquityBySymbol(@RequestParam("SymbolProvider")                  String symbolProvider,
                                                      @RequestParam("Symbol")                          String symbol,
                                                      @RequestParam(value = "Venue", required = false) String venue)
    {
        return productService.getEquityBySymbol(symbolProvider, symbol, venue);
    }

    /**
     * Retrieves a {@link FutureProductInstrument} when given a symbol and an optional venue
     *
     * @param symbolProvider The source of this symbol
     * @param symbol         A code used by the provider to reference a specific futures contract
     * @param venue          An optional venue - some provider symbols are only unique per venue
     * @return A {@link FutureProductInstrument} matching the symbol or
     */
    @GetMapping(Endpoints.GET_FUTURE_BY_SYMBOL)
    private FutureProductInstrument getFutureBySymbol(@RequestParam("SymbolProvider")                  String symbolProvider,
                                                      @RequestParam("Symbol")                          String symbol,
                                                      @RequestParam(value = "Venue", required = false) String venue)
    {
        return productService.getFutureBySymbol(symbolProvider, symbol, venue);
    }

    /**
     * Retrieves a {@link OptionProductInstrument} when given a symbol and an optional venue
     *
     * @param symbolProvider The source of this symbol
     * @param symbol         A code used by the provider to reference a specific options contract
     * @param venue          An optional venue - some provider symbols are only unique per venue
     * @return A {@link OptionProductInstrument} matching the symbol or
     */
    @GetMapping(Endpoints.GET_OPTION_BY_SYMBOL)
    private OptionProductInstrument getOptionBySymbol(@RequestParam("SymbolProvider")                  String symbolProvider,
                                                      @RequestParam("Symbol")                          String symbol,
                                                      @RequestParam(value = "Venue", required = false) String venue)
    {
        return productService.getOptionBySymbol(symbolProvider, symbol, venue);
    }

    /**
     * Retrieves a list of {@link Venue}s
     *
     * @param mic Can be used to confirm a venue existence (and get a specific venue's info)
     * @param acronym Can be used to confirm a venue existence (and get a specific venue's info)
     * @return A list of {@link Venue}s
     */
    @GetMapping(Endpoints.GET_VENUES)
    private List<Venue> getVenues(@RequestParam("MIC") Optional<String> mic,
                                  @RequestParam("MIC") Optional<String> acronym)
    {
        return productService.getVenues(mic, acronym);
    }

    /**
     * Retrieves a list of {@link Venue}s
     *
     * @param lookups A list of {@link Symbol}s that will have their cross-references retrieved
     *
     * @return A list of {@link Venue}s
     */
    @GetMapping(Endpoints.GET_CROSS_REFERENCES)
    private List<CrossReferences> getCrossReferences(@RequestParam("lookups") Symbol[] lookups)
    {
        Arrays.stream(lookups).forEach(System.out::println);
        return productService.getCrossReferences(Arrays.asList(lookups));
    }

}
