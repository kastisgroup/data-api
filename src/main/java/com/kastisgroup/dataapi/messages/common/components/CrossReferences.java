package com.kastisgroup.dataapi.messages.common.components;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.Serializable;
import java.util.List;

public class CrossReferences implements Serializable
{
    private Symbol       lookup;
    private List<Symbol> crossReferences;

    private CrossReferences(Symbol lookup, List<Symbol> crossReferences)
    {
        this.lookup          = lookup;
        this.crossReferences = crossReferences;
    }

    public Symbol getLookup()
    {
        return lookup;
    }

    public List<Symbol> getCrossReferences()
    {
        return crossReferences;
    }

    public static CrossReferences of(Symbol symbol, List<Symbol> crossReferences)
    {
        return new CrossReferences(symbol, crossReferences);
    }

    public String toJson(ObjectMapper mapper)
    {
        try
        {
            return mapper.writeValueAsString(this);
        }
        catch (JsonProcessingException e)
        {
            return "";
        }
    }

    @Override
    public String toString()
    {
        try
        {
            return new ObjectMapper().writeValueAsString(this);
        }
        catch (JsonProcessingException e)
        {
            e.printStackTrace();
            return "";
        }
    }
}
