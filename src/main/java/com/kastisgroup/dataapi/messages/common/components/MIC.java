package com.kastisgroup.dataapi.messages.common.components;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.Objects;
import java.util.Optional;

public class MIC
{
    String           mic;
    Optional<String> operatingMIC;

    private MIC(String mic, String operatingMIC)
    {
        this.mic          = mic;
        this.operatingMIC = Optional.of(operatingMIC);
    }

    @JsonProperty("mic")
    public String getMic()
    {
        return mic;
    }

    @JsonProperty("operatingMIC")
    public Optional<String> getOperatingMIC()
    {
        return operatingMIC;
    }

    public static MIC of(String mic, String operatingMIC)
    {
        return new MIC(mic, operatingMIC);
    }

    @Override
    public boolean equals(Object o)
    {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        MIC mic1 = (MIC) o;
        return Objects.equals(mic, mic1.mic) &&
                Objects.equals(operatingMIC, mic1.operatingMIC);
    }

    @Override
    public int hashCode()
    {
        return Objects.hash(mic, operatingMIC);
    }

    @Override
    public String toString()
    {
        return operatingMIC.isPresent()?(operatingMIC.get()+":"+mic):mic;
    }
}
