package com.kastisgroup.dataapi.messages.common.components;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.util.Objects;

public class Symbol
{
    String symbolProvider;
    String symbol;

    private Symbol(String symbolProvider, String symbol)
    {
        this.symbolProvider = symbolProvider;
        this.symbol         = symbol;
    }

    @JsonCreator
    public static Symbol of(@JsonProperty("symbolProvider") String symbolProvider,
                            @JsonProperty("symbol")         String symbol)
    {
        return new Symbol(symbolProvider, symbol);
    }

    @JsonProperty("symbolProvider")
    public String getSymbolProvider()
    {
        return symbolProvider;
    }

    @JsonProperty("symbol")
    public String getSymbol()
    {
        return symbol;
    }

    @Override
    public boolean equals(Object o)
    {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Symbol symbol1 = (Symbol) o;
        return Objects.equals(symbolProvider, symbol1.symbolProvider) &&
                Objects.equals(symbol, symbol1.symbol);
    }

    @Override
    public int hashCode()
    {
        return Objects.hash(symbolProvider, symbol);
    }

    @Override
    public String toString()
    {
        return symbolProvider+":"+symbol;
    }

    public static Symbol fromString(String asString)
    {
        if (asString == null)
            return null;

        String[] components = asString.split(":");
        if (components.length != 2)
            return null;

        return of(components[0], components[1]);
    }

    public String toJson(ObjectMapper mapper)
    {
        try
        {
            return mapper.writeValueAsString(this);
        }
        catch (JsonProcessingException e)
        {
            return "";
        }
    }
}
