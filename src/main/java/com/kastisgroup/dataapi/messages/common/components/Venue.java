package com.kastisgroup.dataapi.messages.common.components;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonUnwrapped;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.util.Objects;

public class Venue
{
    private String acronym;
    private String fullName;
    private MIC    mic;

    private Venue(MIC mic, String acronym, String fullName)
    {
        this.mic      = mic;
        this.acronym  = acronym;
        this.fullName = fullName;
    }

    @JsonProperty("acronym")
    public String getAcronym()
    {
        return acronym;
    }

    @JsonProperty("fullName")
    public String getFullName()
    {
        return fullName;
    }

    @JsonUnwrapped
    public MIC getMic()
    {
        return mic;
    }

    @Override
    public boolean equals(Object o)
    {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Venue venue = (Venue) o;
        return Objects.equals(acronym, venue.acronym) &&
                Objects.equals(fullName, venue.fullName) &&
                Objects.equals(mic, venue.mic);
    }

    @Override
    public int hashCode()
    {
        return Objects.hash(acronym, fullName, mic);
    }

    public String toJson(ObjectMapper mapper)
    {
        try
        {
            return mapper.writeValueAsString(this);
        }
        catch (JsonProcessingException e)
        {
            return "";
        }
    }

    public static final class Builder
    {
        private MIC    mic;
        private String acronym;
        private String fullName;

        private Builder()
        {
        }

        public static Builder aVenue()
        {
            return new Builder();
        }

        public Builder withMic(MIC mic)
        {
            this.mic = mic;
            return this;
        }

        public Builder withAcronym(String acronym)
        {
            this.acronym = acronym;
            return this;
        }

        public Builder withFullName(String fullName)
        {
            this.fullName = fullName;
            return this;
        }

        public Venue build()
        {
            return new Venue(mic, acronym, fullName);
        }
    }
}
