package com.kastisgroup.dataapi.messages.common.converters;

import com.kastisgroup.dataapi.messages.common.components.Symbol;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

@Component
public class StringToSymbol implements Converter<String, Symbol>
{
    @Override
    public Symbol convert(String asString)
    {
        return Symbol.fromString(asString);
    }
}
