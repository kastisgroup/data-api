package com.kastisgroup.dataapi.messages.order;

import com.fasterxml.jackson.annotation.JsonUnwrapped;
import com.kastisgroup.dataapi.messages.common.components.Symbol;
import com.kastisgroup.dataapi.messages.order.enums.OrderState;
import com.kastisgroup.dataapi.messages.order.enums.Side;

import java.math.BigDecimal;

public class Order
{
    private String     clientOrderId;
    private String     systemOrderId;
    private String     clientReplacementId;
    private String     systemReplacementId;
    private String     account;
    private Side       side;
    private BigDecimal quantity;
    private Symbol     symbol;
    private PriceType  priceType;
    private BigDecimal price;
    private BigDecimal stopPrice;
    private BigDecimal filledQty;
    private BigDecimal lostQty;
    private OrderState orderState;

    private Order() {}

    public String getClientOrderId()
    {
        return clientOrderId;
    }

    public String getSystemOrderId()
    {
        return systemOrderId;
    }

    public String getClientReplacementId()
    {
        return clientReplacementId;
    }

    public String getSystemReplacementId()
    {
        return systemReplacementId;
    }

    public String getAccount()
    {
        return account;
    }

    public Side getSide()
    {
        return side;
    }

    public BigDecimal getQuantity()
    {
        return quantity;
    }

    @JsonUnwrapped
    public Symbol getSymbol()
    {
        return symbol;
    }

    public PriceType getPriceType()
    {
        return priceType;
    }

    public BigDecimal getPrice()
    {
        return price;
    }

    public BigDecimal getStopPrice()
    {
        return stopPrice;
    }

    public BigDecimal getFilledQty()
    {
        return filledQty;
    }

    public BigDecimal getLostQty()
    {
        return lostQty;
    }

    public BigDecimal getOpenQty()
    {
        return quantity.subtract(getFilledQty()).subtract(getLostQty());
    }

    public OrderState getOrderState()
    {
        return orderState;
    }

    public static final class Builder
    {
        private String     clientOrderId;
        private String     systemOrderId;
        private String     clientReplacementId;
        private String     systemReplacementId;
        private String     account;
        private Side       side;
        private BigDecimal quantity;
        private Symbol     symbol;
        private PriceType  priceType;
        private BigDecimal price;
        private BigDecimal stopPrice;
        private BigDecimal filledQty           = BigDecimal.ZERO;
        private BigDecimal lostQty             = BigDecimal.ZERO;
        private OrderState orderState;

        private Builder()
        {
        }

        public static Builder anOrder()
        {
            return new Builder();
        }

        public Builder withClientOrderId(String clientOrderId)
        {
            this.clientOrderId = clientOrderId;
            return this;
        }

        public Builder withSystemOrderId(String systemOrderId)
        {
            this.systemOrderId = systemOrderId;
            return this;
        }

        public Builder withClientReplacementId(String clientReplacementId)
        {
            this.clientReplacementId = clientReplacementId;
            return this;
        }

        public Builder withSystemReplacementId(String systemReplacementId)
        {
            this.systemReplacementId = systemReplacementId;
            return this;
        }

        public Builder withAccount(String account)
        {
            this.account = account;
            return this;
        }

        public Builder withSide(Side side)
        {
            this.side = side;
            return this;
        }

        public Builder withQuantity(BigDecimal quantity)
        {
            this.quantity = quantity;
            return this;
        }

        public Builder withSymbol(Symbol symbol)
        {
            this.symbol = symbol;
            return this;
        }

        public Builder withPriceType(PriceType priceType)
        {
            this.priceType = priceType;
            return this;
        }

        public Builder withPrice(BigDecimal price)
        {
            this.price = price;
            return this;
        }

        public Builder withStopPrice(BigDecimal stopPrice)
        {
            this.stopPrice = stopPrice;
            return this;
        }

        public Builder withFilledQty(BigDecimal filledQty)
        {
            this.filledQty = filledQty;
            return this;
        }

        public Builder withLostQty(BigDecimal lostQty)
        {
            this.lostQty = lostQty;
            return this;
        }

        public Builder withOrderState(OrderState orderState)
        {
            this.orderState = orderState;
            return this;
        }

        public Order build()
        {
            Order order = new Order();

            order.clientOrderId       = this.clientOrderId;
            order.systemOrderId       = this.systemOrderId;
            order.clientReplacementId = this.clientReplacementId;
            order.systemReplacementId = this.systemReplacementId;
            order.account             = this.account;
            order.side                = this.side;
            order.symbol              = this.symbol;
            order.quantity            = this.quantity;
            order.priceType           = this.priceType;
            order.price               = this.price;
            order.stopPrice           = this.stopPrice;
            order.filledQty           = this.filledQty;
            order.lostQty             = this.lostQty;
            order.orderState          = this.orderState;

            return order;
        }
    }
}
