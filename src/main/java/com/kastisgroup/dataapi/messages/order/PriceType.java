package com.kastisgroup.dataapi.messages.order;

public enum PriceType
{
    MARKET, LIMIT, STOP, STOP_LIMIT;

    public String asJson() {return name();}
}
