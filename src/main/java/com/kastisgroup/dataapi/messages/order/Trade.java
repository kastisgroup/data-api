package com.kastisgroup.dataapi.messages.order;

import com.kastisgroup.dataapi.messages.common.components.Symbol;
import com.kastisgroup.dataapi.messages.order.enums.Side;

import java.math.BigDecimal;

public class Trade
{
    private String     clientOrderId;
    private String     systemOrderId;
    private Symbol     symbol;
    private String     account;
    private Side       side;
    private BigDecimal quantity;
    private BigDecimal price;
    private BigDecimal cumulativeQty;
    private BigDecimal openQty;

    private Trade() {}

    public String getClientOrderId()
    {
        return clientOrderId;
    }

    public String getSystemOrderId()
    {
        return systemOrderId;
    }

    public Symbol getSymbol()
    {
        return symbol;
    }

    public String getAccount()
    {
        return account;
    }

    public Side getSide()
    {
        return side;
    }

    public BigDecimal getQuantity()
    {
        return quantity;
    }

    public BigDecimal getPrice()
    {
        return price;
    }

    public BigDecimal getCumulativeQty()
    {
        return cumulativeQty;
    }

    public BigDecimal getOpenQty()
    {
        return openQty;
    }


    public static final class Builder
    {
        private String     clientOrderId;
        private String     systemOrderId;
        private Symbol     symbol;
        private String     account;
        private Side       side;
        private BigDecimal quantity;
        private BigDecimal price;
        private BigDecimal cumulativeQty;
        private BigDecimal openQty;

        private Builder()
        {
        }

        public static Builder aTrade()
        {
            return new Builder();
        }

        public Builder withClientOrderId(String clientOrderId)
        {
            this.clientOrderId = clientOrderId;
            return this;
        }

        public Builder withSystemOrderId(String systemOrderId)
        {
            this.systemOrderId = systemOrderId;
            return this;
        }

        public Builder withSymbol(Symbol symbol)
        {
            this.symbol = symbol;
            return this;
        }

        public Builder withAccount(String account)
        {
            this.account = account;
            return this;
        }

        public Builder withSide(Side side)
        {
            this.side = side;
            return this;
        }

        public Builder withQuantity(BigDecimal quantity)
        {
            this.quantity = quantity;
            return this;
        }

        public Builder withPrice(BigDecimal price)
        {
            this.price = price;
            return this;
        }

        public Builder withCumulativeQty(BigDecimal cumulativeQty)
        {
            this.cumulativeQty = cumulativeQty;
            return this;
        }

        public Builder withOpenQty(BigDecimal openQty)
        {
            this.openQty = openQty;
            return this;
        }

        public Trade build()
        {
            Trade trade = new Trade();

            trade.account       = this.account;
            trade.clientOrderId = this.clientOrderId;
            trade.symbol        = this.symbol;
            trade.systemOrderId = this.systemOrderId;
            trade.price         = this.price;
            trade.openQty       = this.openQty;
            trade.quantity      = this.quantity;
            trade.cumulativeQty = this.cumulativeQty;
            trade.side          = this.side;
            
            return trade;
        }
    }
}
