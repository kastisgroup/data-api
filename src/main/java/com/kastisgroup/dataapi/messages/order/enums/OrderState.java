package com.kastisgroup.dataapi.messages.order.enums;

public enum OrderState
{
    CREATED(true),
    SUBMITTED(true),
    WORKING(true),
    FILLED(false),
    CANCELED(false),
    REPLACED(false),
    REJECTED(false),
    DONE_FOR_DAY(false),
    EXPIRED(false),
    SUSPENDED(false),
    STOPPED(false);

    private boolean open;

    private OrderState(boolean open)
    {
        this.open = open;
    }

    public boolean isOpen()
    {
        return open;
    }

    public boolean isClosed()
    {
        return !open;
    }

    public String asJson() {return name();}
}
