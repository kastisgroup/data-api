package com.kastisgroup.dataapi.messages.order.enums;

import com.fasterxml.jackson.annotation.JsonProperty;

public enum Side
{
    @JsonProperty("B")
    BUY("B"),
    @JsonProperty("S")
    SELL("S");

    private String code;

    private Side(String code)
    {
        this.code = code;
    }

    public String getCode()
    {
        return code;
    }

    public String asJsonValue() {return code;}
}
