package com.kastisgroup.dataapi.messages.product;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.kastisgroup.dataapi.messages.product.components.CFI;
import com.neovisionaries.i18n.CurrencyCode;

import java.math.BigDecimal;
import java.time.LocalDate;

public class EquityInstrument extends Instrument
{
    protected CurrencyCode tradingCurrency;
    protected CurrencyCode quotationCurrency;

    public CurrencyCode getTradingCurrency()
    {
        return tradingCurrency;
    }

    public CurrencyCode getQuotationCurrency()
    {
        return quotationCurrency;
    }

    @JsonProperty("tradingCurrency")
    public String getTradingCurrencyCode()
    {
        return tradingCurrency.name();
    }

    @JsonProperty("quotationCurrency")
    public String getQuotationCurrencyCode()
    {
        return quotationCurrency.name();
    }

    public static final class Builder
    {
        protected long         id;
        protected CurrencyCode tradingCurrency;
        protected CurrencyCode quotationCurrency;
        protected LocalDate    firstTradingDate;
        protected LocalDate    lastTradingDate;
        protected CFI          cfi;
        protected BigDecimal   contractSize;
        protected BigDecimal   multiplier;

        private Builder()
        {
        }

        public static Builder anEquityInstrument()
        {
            return new Builder();
        }

        public Builder withId(long id)
        {
            this.id = id;
            return this;
        }

        public Builder withTradingCurrency(CurrencyCode tradingCurrency)
        {
            this.tradingCurrency = tradingCurrency;
            return this;
        }

        public Builder withQuotationCurrency(CurrencyCode quotationCurrency)
        {
            this.quotationCurrency = quotationCurrency;
            return this;
        }

        public Builder withFirstTradingDate(LocalDate firstTradingDate)
        {
            this.firstTradingDate = firstTradingDate;
            return this;
        }

        public Builder withLastTradingDate(LocalDate lastTradingDate)
        {
            this.lastTradingDate = lastTradingDate;
            return this;
        }

        public Builder withCFI(CFI cfi)
        {
            this.cfi = cfi;
            return this;
        }

        public Builder withContractSize(BigDecimal contractSize)
        {
            this.contractSize = contractSize;
            return this;
        }

        public Builder withMultiplier(BigDecimal multiplier)
        {
            this.multiplier = multiplier;
            return this;
        }

        public EquityInstrument build()
        {
            EquityInstrument equityInstrument = new EquityInstrument();

            equityInstrument.lastTradingDate   = this.lastTradingDate;
            equityInstrument.quotationCurrency = this.quotationCurrency;
            equityInstrument.multiplier        = this.multiplier;
            equityInstrument.tradingCurrency   = this.tradingCurrency;
            equityInstrument.cfi               = this.cfi;
            equityInstrument.id                = this.id;
            equityInstrument.firstTradingDate  = this.firstTradingDate;
            equityInstrument.contractSize      = this.contractSize;

            return equityInstrument;
        }
    }
}
