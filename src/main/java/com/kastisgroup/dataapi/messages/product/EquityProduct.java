package com.kastisgroup.dataapi.messages.product;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.kastisgroup.dataapi.messages.product.enums.AssetCategory;
import com.kastisgroup.dataapi.messages.product.enums.AssetClass;
import com.neovisionaries.i18n.CountryCode;
import com.neovisionaries.i18n.CurrencyCode;

import java.math.BigDecimal;

public class EquityProduct extends Product
{
    protected CountryCode countryOfIssue;

    @JsonIgnore
    public CountryCode getCountryOfIssue()
    {
        return countryOfIssue;
    }

    @JsonProperty("countryOfIssue")
    public String getCountryOfIssueAlpha3()
    {
        return countryOfIssue.getAlpha3();
    }


    public static final class Builder
    {
        protected long          id;
        protected AssetClass    assetClass;
        protected AssetCategory assetCategory;
        protected CurrencyCode  currency;
        protected BigDecimal    contractSize;
        protected CountryCode   countryOfIssue;
        protected BigDecimal    lotSize;
        protected BigDecimal    multiplier;
        protected BigDecimal    tickSize;

        private Builder()
        {
        }

        public static Builder anEquityProduct()
        {
            return new Builder();
        }

        public Builder withId(long id)
        {
            this.id = id;
            return this;
        }

        public Builder withAssetClass(AssetClass assetClass)
        {
            this.assetClass = assetClass;
            return this;
        }

        public Builder withAssetCategory(AssetCategory assetCategory)
        {
            this.assetCategory = assetCategory;
            return this;
        }

        public Builder withCurrency(CurrencyCode currency)
        {
            this.currency = currency;
            return this;
        }

        public Builder withContractSize(BigDecimal contractSize)
        {
            this.contractSize = contractSize;
            return this;
        }

        public Builder withCountryOfIssue(CountryCode countryOfIssue)
        {
            this.countryOfIssue = countryOfIssue;
            return this;
        }

        public Builder withLotSize(BigDecimal lotSize)
        {
            this.lotSize = lotSize;
            return this;
        }

        public Builder withMultiplier(BigDecimal multiplier)
        {
            this.multiplier = multiplier;
            return this;
        }

        public Builder withTickSize(BigDecimal tickSize)
        {
            this.tickSize = tickSize;
            return this;
        }

        public EquityProduct build()
        {
            EquityProduct equityProduct = new EquityProduct();

            equityProduct.id             = this.id;
            equityProduct.currency       = this.currency;
            equityProduct.assetCategory  = this.assetCategory;
            equityProduct.tickSize       = this.tickSize;
            equityProduct.contractSize   = this.contractSize;
            equityProduct.assetClass     = this.assetClass;
            equityProduct.countryOfIssue = this.countryOfIssue;
            equityProduct.lotSize        = this.lotSize;
            equityProduct.multiplier     = this.multiplier;

            return equityProduct;
        }
    }
}
