package com.kastisgroup.dataapi.messages.product;

import com.kastisgroup.dataapi.messages.common.components.Venue;

import java.util.Collections;
import java.util.List;

public class EquityProductInstrument extends ProductInstrument<EquityProduct, EquityInstrument>
{
    public static final class Builder
    {
        private EquityProduct    product;
        private EquityInstrument instrument;
        private List<Venue>      venues;

        private Builder()
        {
        }

        public static Builder anEquityProductInstrument()
        {
            return new Builder();
        }

        public Builder withProduct(EquityProduct product)
        {
            this.product = product;
            return this;
        }

        public Builder withInstrument(EquityInstrument instrument)
        {
            this.instrument = instrument;
            return this;
        }

        public Builder withVenues(List<Venue> venues)
        {
            this.venues = venues;
            return this;
        }

        public EquityProductInstrument build()
        {
            EquityProductInstrument equityProductInstrument = new EquityProductInstrument();

            equityProductInstrument.product    = this.product;
            equityProductInstrument.instrument = this.instrument;
            equityProductInstrument.venues     = this.venues == null ? Collections.emptyList() : this.venues;

            return equityProductInstrument;
        }
    }
}
