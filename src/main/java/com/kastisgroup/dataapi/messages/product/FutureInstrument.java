package com.kastisgroup.dataapi.messages.product;

import com.kastisgroup.dataapi.messages.product.components.CFI;

import java.math.BigDecimal;
import java.time.LocalDate;

public class FutureInstrument extends Instrument
{
    protected LocalDate  contractDate;
    protected LocalDate  valuationDate;
    protected BigDecimal rate;

    public LocalDate getContractDate()
    {
        return contractDate;
    }

    public LocalDate getValuationDate()
    {
        return valuationDate;
    }

    public BigDecimal getRate()
    {
        return rate;
    }

    public static final class Builder
    {
        protected LocalDate  contractDate;
        protected long       id;
        protected LocalDate  valuationDate;
        protected LocalDate  firstTradingDate;
        protected BigDecimal rate;
        protected LocalDate  lastTradingDate;
        protected CFI        cfi;
        protected BigDecimal contractSize;
        protected BigDecimal multiplier;

        private Builder()
        {
        }

        public static Builder aFutureInstrument()
        {
            return new Builder();
        }

        public Builder withContractDate(LocalDate contractDate)
        {
            this.contractDate = contractDate;
            return this;
        }

        public Builder withId(long id)
        {
            this.id = id;
            return this;
        }

        public Builder withValuationDate(LocalDate valuationDate)
        {
            this.valuationDate = valuationDate;
            return this;
        }

        public Builder withFirstTradingDate(LocalDate firstTradingDate)
        {
            this.firstTradingDate = firstTradingDate;
            return this;
        }

        public Builder withRate(BigDecimal rate)
        {
            this.rate = rate;
            return this;
        }

        public Builder withLastTradingDate(LocalDate lastTradingDate)
        {
            this.lastTradingDate = lastTradingDate;
            return this;
        }

        public Builder withCFI(CFI cfi)
        {
            this.cfi = cfi;
            return this;
        }

        public Builder withContractSize(BigDecimal contractSize)
        {
            this.contractSize = contractSize;
            return this;
        }

        public Builder withMultiplier(BigDecimal multiplier)
        {
            this.multiplier = multiplier;
            return this;
        }

        public FutureInstrument build()
        {
            FutureInstrument futureInstrument = new FutureInstrument();

            futureInstrument.firstTradingDate = this.firstTradingDate;
            futureInstrument.contractSize     = this.contractSize;
            futureInstrument.valuationDate    = this.valuationDate;
            futureInstrument.multiplier       = this.multiplier;
            futureInstrument.rate             = this.rate;
            futureInstrument.contractDate     = this.contractDate;
            futureInstrument.cfi              = this.cfi;
            futureInstrument.id               = this.id;
            futureInstrument.lastTradingDate  = this.lastTradingDate;

            return futureInstrument;
        }
    }
}
