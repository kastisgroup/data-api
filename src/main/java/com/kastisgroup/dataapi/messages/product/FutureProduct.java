package com.kastisgroup.dataapi.messages.product;

import com.kastisgroup.dataapi.messages.product.enums.AssetCategory;
import com.kastisgroup.dataapi.messages.product.enums.AssetClass;
import com.kastisgroup.dataapi.messages.product.enums.DerivativeType;
import com.kastisgroup.dataapi.messages.product.enums.SettlementType;
import com.neovisionaries.i18n.CurrencyCode;

import java.math.BigDecimal;

public class FutureProduct extends Product
{
    private final DerivativeType derivativeType;
    private final SettlementType settlementType;

    private FutureProduct(DerivativeType derivativeType, SettlementType settlementType)
    {
        this.derivativeType = derivativeType;
        this.settlementType = settlementType;
    }

    public DerivativeType getDerivativeType()
    {
        return derivativeType;
    }

    public SettlementType getSettlementType()
    {
        return settlementType;
    }

    public static final class Builder
    {
        protected long           id;
        protected AssetClass     assetClass;
        protected AssetCategory  assetCategory;
        protected CurrencyCode   currency;
        protected BigDecimal     contractSize;
        protected BigDecimal     lotSize;
        protected BigDecimal     multiplier;
        protected BigDecimal     tickSize;
        private   DerivativeType derivativeType;
        private   SettlementType settlementType;

        private Builder()
        {
        }

        public static Builder aFutureProduct()
        {
            return new Builder();
        }

        public Builder withDerivativeType(DerivativeType derivativeType)
        {
            this.derivativeType = derivativeType;
            return this;
        }

        public Builder withSettlementType(SettlementType settlementType)
        {
            this.settlementType = settlementType;
            return this;
        }

        public Builder withId(long id)
        {
            this.id = id;
            return this;
        }

        public Builder withAssetClass(AssetClass assetClass)
        {
            this.assetClass = assetClass;
            return this;
        }

        public Builder withAssetCategory(AssetCategory assetCategory)
        {
            this.assetCategory = assetCategory;
            return this;
        }

        public Builder withCurrency(CurrencyCode currency)
        {
            this.currency = currency;
            return this;
        }

        public Builder withContractSize(BigDecimal contractSize)
        {
            this.contractSize = contractSize;
            return this;
        }

        public Builder withLotSize(BigDecimal lotSize)
        {
            this.lotSize = lotSize;
            return this;
        }

        public Builder withMultiplier(BigDecimal multiplier)
        {
            this.multiplier = multiplier;
            return this;
        }

        public Builder withTickSize(BigDecimal tickSize)
        {
            this.tickSize = tickSize;
            return this;
        }

        public FutureProduct build()
        {
            FutureProduct futureProduct = new FutureProduct(derivativeType, settlementType);

            futureProduct.currency      = this.currency;
            futureProduct.assetCategory = this.assetCategory;
            futureProduct.tickSize      = this.tickSize;
            futureProduct.id            = this.id;
            futureProduct.contractSize  = this.contractSize;
            futureProduct.assetClass    = this.assetClass;
            futureProduct.lotSize       = this.lotSize;
            futureProduct.multiplier    = this.multiplier;

            return futureProduct;
        }
    }
}
