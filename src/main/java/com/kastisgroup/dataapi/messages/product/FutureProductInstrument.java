package com.kastisgroup.dataapi.messages.product;

import com.kastisgroup.dataapi.messages.common.components.Venue;

import java.util.Collections;
import java.util.List;

public class FutureProductInstrument extends ProductInstrument<FutureProduct, FutureInstrument>
{
    public static final class Builder
    {
        private FutureProduct    product;
        private FutureInstrument instrument;
        private List<Venue>      venues;

        private Builder()
        {
        }

        public static Builder anFutureProductInstrument()
        {
            return new Builder();
        }

        public Builder withProduct(FutureProduct product)
        {
            this.product = product;
            return this;
        }

        public Builder withInstrument(FutureInstrument instrument)
        {
            this.instrument = instrument;
            return this;
        }

        public Builder withVenues(List<Venue> venues)
        {
            this.venues = venues;
            return this;
        }

        public FutureProductInstrument build()
        {
            FutureProductInstrument futureProductInstrument = new FutureProductInstrument();

            futureProductInstrument.product    = this.product;
            futureProductInstrument.instrument = this.instrument;
            futureProductInstrument.venues     = this.venues == null ? Collections.emptyList() : this.venues;

            return futureProductInstrument;
        }
    }
}
