package com.kastisgroup.dataapi.messages.product;

import com.fasterxml.jackson.annotation.JsonUnwrapped;
import com.kastisgroup.dataapi.messages.product.components.CFI;
import org.springframework.data.annotation.Id;

import java.math.BigDecimal;
import java.time.LocalDate;

public abstract class Instrument
{
    @Id
    protected long id;

    protected LocalDate  firstTradingDate;
    protected LocalDate  lastTradingDate;
    @JsonUnwrapped
    protected CFI        cfi;
    protected BigDecimal contractSize;
    protected BigDecimal multiplier;

    public long getId()
    {
        return id;
    }

    public LocalDate getFirstTradingDate()
    {
        return firstTradingDate;
    }

    public LocalDate getLastTradingDate()
    {
        return lastTradingDate;
    }

    public CFI getCFI()
    {
        return cfi;
    }

    public BigDecimal getContractSize()
    {
        return contractSize;
    }

    public BigDecimal getMultiplier()
    {
        return multiplier;
    }
}
