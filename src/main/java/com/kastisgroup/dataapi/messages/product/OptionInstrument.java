package com.kastisgroup.dataapi.messages.product;

import com.fasterxml.jackson.annotation.JsonUnwrapped;
import com.kastisgroup.dataapi.messages.product.components.CFI;
import com.kastisgroup.dataapi.messages.product.enums.PutCall;

import java.math.BigDecimal;
import java.time.LocalDate;

public class OptionInstrument extends Instrument
{
    @JsonUnwrapped
    protected PutCall    putCall;
    protected BigDecimal strike;
    protected LocalDate  keyDate;
    protected LocalDate  contractDate;
    protected LocalDate  expirationDate;

    public PutCall getPutCall()
    {
        return putCall;
    }

    public BigDecimal getStrike()
    {
        return strike;
    }

    public LocalDate getKeyDate()
    {
        return keyDate;
    }

    public LocalDate getContractDate()
    {
        return contractDate;
    }

    public LocalDate getExpirationDate()
    {
        return expirationDate;
    }

    public static final class Builder
    {
        protected long       id;
        protected LocalDate  firstTradingDate;
        protected LocalDate  lastTradingDate;
        protected CFI        cfi;
        protected BigDecimal contractSize;
        protected BigDecimal multiplier;
        protected PutCall    putCall;
        protected BigDecimal strike;
        protected LocalDate  keyDate;
        protected LocalDate  contractDate;
        protected LocalDate  expirationDate;

        private Builder()
        {
        }

        public static Builder anOptionInstrument()
        {
            return new Builder();
        }

        public Builder withPutCall(PutCall putCall)
        {
            this.putCall = putCall;
            return this;
        }

        public Builder withId(long id)
        {
            this.id = id;
            return this;
        }

        public Builder withStrike(BigDecimal strike)
        {
            this.strike = strike;
            return this;
        }

        public Builder withFirstTradingDate(LocalDate firstTradingDate)
        {
            this.firstTradingDate = firstTradingDate;
            return this;
        }

        public Builder withKeyDate(LocalDate keyDate)
        {
            this.keyDate = keyDate;
            return this;
        }

        public Builder withLastTradingDate(LocalDate lastTradingDate)
        {
            this.lastTradingDate = lastTradingDate;
            return this;
        }

        public Builder withContractDate(LocalDate contractDate)
        {
            this.contractDate = contractDate;
            return this;
        }

        public Builder withExpirationDate(LocalDate expirationDate)
        {
            this.expirationDate = expirationDate;
            return this;
        }

        public Builder withCFI(CFI cfi)
        {
            this.cfi = cfi;
            return this;
        }

        public Builder withContractSize(BigDecimal contractSize)
        {
            this.contractSize = contractSize;
            return this;
        }

        public Builder withMultiplier(BigDecimal multiplier)
        {
            this.multiplier = multiplier;
            return this;
        }

        public OptionInstrument build()
        {
            OptionInstrument optionInstrument = new OptionInstrument();

            optionInstrument.expirationDate   = this.expirationDate;
            optionInstrument.lastTradingDate  = this.lastTradingDate;
            optionInstrument.keyDate          = this.keyDate;
            optionInstrument.multiplier       = this.multiplier;
            optionInstrument.cfi              = this.cfi;
            optionInstrument.id               = this.id;
            optionInstrument.contractDate     = this.contractDate;
            optionInstrument.strike           = this.strike;
            optionInstrument.firstTradingDate = this.firstTradingDate;
            optionInstrument.contractSize     = this.contractSize;
            optionInstrument.putCall          = this.putCall;

            return optionInstrument;
        }
    }
}
