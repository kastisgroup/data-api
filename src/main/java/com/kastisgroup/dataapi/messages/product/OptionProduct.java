package com.kastisgroup.dataapi.messages.product;

import com.kastisgroup.dataapi.messages.product.enums.*;
import com.neovisionaries.i18n.CurrencyCode;

import java.math.BigDecimal;

public class OptionProduct extends Product
{
    private final ExerciseStyle  exerciseStyle;
    private final DerivativeType derivativeType;
    private final SettlementType settlementType;
    private final CurrencyCode   strikeCurrency;
    private final BigDecimal     strikePriceMultipler;
    private final BigDecimal     strikePriceShift;
    private final Integer        version;
    private final Integer        generation;

    private OptionProduct(ExerciseStyle  exerciseStyle,        DerivativeType derivativeType,
                          SettlementType settlementType,       CurrencyCode   strikeCurrency,
                          BigDecimal     strikePriceMultipler, BigDecimal     strikePriceShift,
                          Integer        version,              Integer        generation)
    {
        this.exerciseStyle        = exerciseStyle;
        this.derivativeType       = derivativeType;
        this.settlementType       = settlementType;
        this.strikeCurrency       = strikeCurrency;
        this.strikePriceMultipler = strikePriceMultipler;
        this.strikePriceShift     = strikePriceShift;
        this.version              = version;
        this.generation           = generation;
    }

    public ExerciseStyle getExerciseStyle()
    {
        return exerciseStyle;
    }

    public DerivativeType getDerivativeType()
    {
        return derivativeType;
    }

    public SettlementType getSettlementType()
    {
        return settlementType;
    }

    public CurrencyCode getStrikeCurrency()
    {
        return strikeCurrency;
    }

    public String getStrikeCurrencyCode()
    {
        return strikeCurrency.name();
    }

    public BigDecimal getStrikePriceMultipler()
    {
        return strikePriceMultipler;
    }

    public BigDecimal getStrikePriceShift()
    {
        return strikePriceShift;
    }

    public Integer getVersion()
    {
        return version;
    }

    public Integer getGeneration()
    {
        return generation;
    }

    public static final class Builder
    {
        protected long           id;
        protected AssetClass     assetClass;
        protected AssetCategory  assetCategory;
        protected CurrencyCode   currency;
        protected BigDecimal     contractSize;
        protected BigDecimal     lotSize;
        protected BigDecimal     multiplier;
        protected BigDecimal     tickSize;
        private   ExerciseStyle  exerciseStyle;
        private   DerivativeType derivativeType;
        private   SettlementType settlementType;
        private   CurrencyCode   strikeCurrency;
        private   BigDecimal     strikePriceMultipler;
        private   BigDecimal     strikePriceShift;
        private   Integer        version;
        private   Integer        generation;

        private Builder()
        {
        }

        public static Builder anOptionProduct()
        {
            return new Builder();
        }

        public Builder withExerciseStyle(ExerciseStyle exerciseStyle)
        {
            this.exerciseStyle = exerciseStyle;
            return this;
        }

        public Builder withId(long id)
        {
            this.id = id;
            return this;
        }

        public Builder withDerivativeType(DerivativeType derivativeType)
        {
            this.derivativeType = derivativeType;
            return this;
        }

        public Builder withAssetClass(AssetClass assetClass)
        {
            this.assetClass = assetClass;
            return this;
        }

        public Builder withAssetCategory(AssetCategory assetCategory)
        {
            this.assetCategory = assetCategory;
            return this;
        }

        public Builder withSettlementType(SettlementType settlementType)
        {
            this.settlementType = settlementType;
            return this;
        }

        public Builder withCurrency(CurrencyCode currency)
        {
            this.currency = currency;
            return this;
        }

        public Builder withStrikeCurrency(CurrencyCode strikeCurrency)
        {
            this.strikeCurrency = strikeCurrency;
            return this;
        }

        public Builder withContractSize(BigDecimal contractSize)
        {
            this.contractSize = contractSize;
            return this;
        }

        public Builder withStrikePriceMultipler(BigDecimal strikePriceMultipler)
        {
            this.strikePriceMultipler = strikePriceMultipler;
            return this;
        }

        public Builder withLotSize(BigDecimal lotSize)
        {
            this.lotSize = lotSize;
            return this;
        }

        public Builder withStrikePriceShift(BigDecimal strikePriceShift)
        {
            this.strikePriceShift = strikePriceShift;
            return this;
        }

        public Builder withMultiplier(BigDecimal multiplier)
        {
            this.multiplier = multiplier;
            return this;
        }

        public Builder withTickSize(BigDecimal tickSize)
        {
            this.tickSize = tickSize;
            return this;
        }

        public Builder withVersion(Integer version)
        {
            this.version = version;
            return this;
        }

        public Builder withGeneration(Integer generation)
        {
            this.generation = generation;
            return this;
        }

        public OptionProduct build()
        {
            OptionProduct optionProduct = new OptionProduct(exerciseStyle,  derivativeType,       settlementType,
                                                            strikeCurrency, strikePriceMultipler, strikePriceShift,
                                                            version,        generation);

            optionProduct.id            = this.id;
            optionProduct.assetClass    = this.assetClass;
            optionProduct.assetCategory = this.assetCategory;
            optionProduct.currency      = this.currency;
            optionProduct.contractSize  = this.contractSize;
            optionProduct.lotSize       = this.lotSize;
            optionProduct.multiplier    = this.multiplier;
            optionProduct.tickSize      = this.tickSize;

            return optionProduct;
        }
    }
}
