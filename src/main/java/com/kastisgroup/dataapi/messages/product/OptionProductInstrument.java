package com.kastisgroup.dataapi.messages.product;

import com.kastisgroup.dataapi.messages.common.components.Venue;

import java.util.Collections;
import java.util.List;

public class OptionProductInstrument extends ProductInstrument<OptionProduct, OptionInstrument>
{
    public static final class Builder
    {
        private OptionProduct    product;
        private OptionInstrument instrument;
        private List<Venue>      venues;

        private Builder()
        {
        }

        public static Builder anOptionProductInstrument()
        {
            return new OptionProductInstrument.Builder();
        }

        public Builder withProduct(OptionProduct product)
        {
            this.product = product;
            return this;
        }

        public Builder withInstrument(OptionInstrument instrument)
        {
            this.instrument = instrument;
            return this;
        }

        public Builder withVenues(List<Venue> venues)
        {
            this.venues = venues;
            return this;
        }

        public OptionProductInstrument build()
        {
            OptionProductInstrument optionProductInstrument = new OptionProductInstrument();

            optionProductInstrument.product    = this.product;
            optionProductInstrument.instrument = this.instrument;
            optionProductInstrument.venues     = this.venues == null ? Collections.emptyList() : this.venues;

            return optionProductInstrument;
        }
    }
}
