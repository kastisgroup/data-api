package com.kastisgroup.dataapi.messages.product;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.kastisgroup.dataapi.messages.product.enums.AssetCategory;
import com.kastisgroup.dataapi.messages.product.enums.AssetClass;
import com.neovisionaries.i18n.CurrencyCode;

import java.math.BigDecimal;

public abstract class Product
{
    protected long          id;
    protected AssetClass    assetClass;
    protected AssetCategory assetCategory;
    protected CurrencyCode  currency;
    protected BigDecimal    contractSize;
    protected BigDecimal    lotSize;
    protected BigDecimal    multiplier;
    protected BigDecimal    tickSize;

    public long getId()
    {
        return id;
    }

    public AssetClass getAssetClass()
    {
        return assetClass;
    }

    public AssetCategory getAssetCategory()
    {
        return assetCategory;
    }

    @JsonIgnore
    public CurrencyCode getCurrency()
    {
        return currency;
    }

    @JsonProperty("currency")
    public String getCurrencyCode()
    {
        return currency.name();
    }

    public BigDecimal getContractSize()
    {
        return contractSize;
    }

    public BigDecimal getLotSize()
    {
        return lotSize;
    }

    public BigDecimal getMultiplier()
    {
        return multiplier;
    }

    public BigDecimal getTickSize()
    {
        return tickSize;
    }
}
