package com.kastisgroup.dataapi.messages.product;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.kastisgroup.dataapi.messages.common.components.Venue;

import java.util.List;

public abstract class ProductInstrument<P extends Product, I extends Instrument>
{
    protected P           product;
    protected I           instrument;
    protected List<Venue> venues;

    protected ProductInstrument() {}

    @JsonProperty("product")
    public P getProduct()
    {
        return product;
    }

    @JsonProperty("instrument")
    public I getInstrument()
    {
        return instrument;
    }

    @JsonProperty("venues")
    public List<Venue> getVenues() {return venues;}
}
