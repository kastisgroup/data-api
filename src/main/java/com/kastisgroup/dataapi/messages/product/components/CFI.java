package com.kastisgroup.dataapi.messages.product.components;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.Objects;

public class CFI
{
    private String code = null;

    private CFI(String code)
    {
        this.code = code;
    }

    public static CFI of(String code)
    {
        return new CFI(validate(code));
    }

    @JsonProperty("cfiCode")
    public String getCode()
    {
        return code;
    }

    private static String validate(String code)
    {
        if (code == null)
            throw new IllegalArgumentException("CFI code must not be null");

        // XXX Add a regex that will validate a proper 6 character code

        return code;
    }

    @Override
    public boolean equals(Object o)
    {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        var cfi = (CFI) o;

        return Objects.equals(code, cfi.code);
    }

    @Override
    public int hashCode()
    {
        return Objects.hash(code);
    }

    @Override
    public String toString()
    {
        return code;
    }
}
