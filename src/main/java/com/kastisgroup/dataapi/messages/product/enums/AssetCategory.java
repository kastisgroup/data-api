package com.kastisgroup.dataapi.messages.product.enums;

import com.fasterxml.jackson.annotation.JsonProperty;
import org.springframework.data.annotation.Id;

public enum AssetCategory
{
    @JsonProperty("ORDINARY_SHARES")
    ORDINARY_SHARES(1, "Ordinary Shares"),
    @JsonProperty("PREFERRED")
    PREFERRED(2, "Preferred"),
    @JsonProperty("ETF")
    ETF(3, "Exchange Traded Fund"),
    @JsonProperty("ETV")
    ETV(4, "Exchange Traded Vehicle"),
    @JsonProperty("INDEX")
    INDEX(5, "Index"),
    @JsonProperty("REIT")
    REIT(6, "Real Estate Investment Trust"),
    @JsonProperty("UNITS")
    UNITS(7, "Units"),
    @JsonProperty("RIGHTS")
    RIGHTS(8, "Rights");

    @Id
    private int    id;
    private String readableName;

    AssetCategory(int id, String readableName)
    {
        this.id           = id;
        this.readableName = readableName;
    }

    public int getId()
    {
        return id;
    }

    public String getReadableName()
    {
        return readableName;
    }

    public String asJsonValue()
    {
        return name();
    }

    @Override
    public String toString()
    {
        return name();
    }
}
