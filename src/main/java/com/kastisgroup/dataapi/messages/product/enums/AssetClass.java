package com.kastisgroup.dataapi.messages.product.enums;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Specifies different asset types like equities, futures, options, debt, FX, etc.
 */
public enum AssetClass
{
    @JsonProperty("EQUITY")
    EQUITY(1),
    @JsonProperty("FUTURE")
    FUTURE(2),
    @JsonProperty("OPTION")
    OPTION(3),
    @JsonProperty("WARRANT")
    WARRANT(4),
    @JsonProperty("DEBT")
    DEBT(5),
    @JsonProperty("CASH")
    CASH(6),
    @JsonProperty("INDEX")
    INDEX(7),
    @JsonProperty("COMMODITY")
    COMMODITY(8);

    private int    id;

    AssetClass(int id)
    {
        this.id   = id;
    }

    public int getId()
    {
        return id;
    }
    public String asJsonValue() {return name();}

    @Override
    public String toString()
    {
        return name();
    }
}
