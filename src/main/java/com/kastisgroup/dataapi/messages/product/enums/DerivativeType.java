package com.kastisgroup.dataapi.messages.product.enums;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.EnumSet;
import java.util.HashMap;
import java.util.Map;

/**
 * Specifies the sub-type of a derivative. There are many types:
 * <ul>
 * <li>Standard     (Standard Derivative Contract)</li>
 * <li>Daily        (Contracts that Settle Daily)</li>
 * <li>Weekly       (Contracts that Settle Weekly)</li>
 * <li>Quaterly     (Contract that Only Occurs Quaterly)</li>
 * <li>Seasonal     (Contract that Only Occurs Seasonally)</li>
 * <li>Semi-annual  (Contract that Only Occurs Semi-annually)</li>
 * <li>Annual       (Contract that Only Occurs Annually)</li>
 * <li>Dividend     (Annual Contract that Pays the Dividend)</li>
 * <li>TAS          (Contract Traded at Settlement)</li>
 * <li>Binary       (Short Term Contracts that Price between 0 and 100 and Settle at 0 or 100)</li>
 * <li>BALMO        (Daily Contracts that Settle on the Last Day of the Month)</li>
 * <li>Rolagem      (Brazilian Spreads Modeled as Futures AKA Rollover)</li>
 * <li>Flex         (Contracts with Custom Attributes (Maturity/Expiry/Settlement/Strike/Style))</li>
 * <li>Mini         (Contract with Reduced Contract Size)</li>
 * <li>Jumbo        (Contract with Increased Contract Size)</li>
 * </ul>
 */
public enum DerivativeType
{
    @JsonProperty("STANDARD")
    STANDARD(1,  "Standard",    "Standard Derivative Contract"),
    @JsonProperty("DAILY")
    DAILY   (2,  "Daily",       "Contracts that Settle Daily"),
    @JsonProperty("WEEKLY")
    WEEKLY  (3,  "Weekly",      "Contracts that Settle Weekly"),
    @JsonProperty("QUATERLY")
    QUATERLY(4,  "Quaterly",    "Contract that Only Occurs Quaterly"),
    @JsonProperty("SEASONAL")
    SEASONAL(5,  "Seasonal",    "Contract that Only Occurs Seasonally"),
    @JsonProperty("SEMI")
    SEMI    (6,  "Semi-annual", "Contract that Only Occurs Semi-annually"),
    @JsonProperty("ANNUAL")
    ANNUAL  (7,  "Annual",      "Contract that Only Occurs Annually"),
    @JsonProperty("DIVIDEND")
    DIVIDEND(8,  "Dividend",    "Annual Contract that Pays the Dividend"),
    @JsonProperty("TAS")
    TAS     (9,  "TAS",         "Contract Traded at Settlement"),
    @JsonProperty("BINARY")
    BINARY  (10, "Binary",      "Short Term Contracts that Price between 0 and 100 and Settle at 0 or 100"),
    @JsonProperty("BALMO")
    BALMO   (11, "BALMO",       "Daily Contracts that Settle on the Last Day of the Month"),
    @JsonProperty("ROLAGEM")
    ROLAGEM (12, "Rolagem",     "Brazilian Spreads Modeled as Futures AKA Rollover"),
    @JsonProperty("FLEX")
    FLEX    (13, "Flex",        "Contracts with Custom Attributes (Maturity/Expiry/Settlement/Strike/Style)"),
    @JsonProperty("MINI")
    MINI    (14, "Mini",        "Contract with Reduced Contract Size"),
    @JsonProperty("JUMBO")
    JUMBO   (15, "Jumbo",       "Contract with Increased Contract Size");

    private static final Map<Integer, DerivativeType> idLookup   = new HashMap<>();
    private static final Map<String, DerivativeType>  jsonLookup = new HashMap<>();

    static {
        for (DerivativeType s : EnumSet.allOf(DerivativeType.class))
        {
            idLookup.put(s.getId(), s);
            jsonLookup.put(s.asJsonValue(), s);
        }
    }

    public static DerivativeType of(String code)
    {
        return jsonLookup.get(code);
    }

    public static DerivativeType of(int id)
    {
        return idLookup.get(id);
    }

    private final int    id;
    private final String json;
    private final String readableName;
    private final String description;

    DerivativeType(int id, String readableName, String description)
    {
        this.id           = id;
        this.json         = this.name();
        this.readableName = readableName;
        this.description  = description;
    }

    public int    getId()           {return id;}
    public String asJsonValue()     {return json;}
    public String getReadableName() {return readableName;}
    public String getDescription()  {return description;}

    @Override
    public String toString()
    {
        return name();
    }
}
