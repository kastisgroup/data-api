package com.kastisgroup.dataapi.messages.product.enums;

import java.util.EnumSet;
import java.util.HashMap;
import java.util.Map;

/**
 * Specifies the exercise style of an option. There are many styles:
 * <ul>
 * <li>American (exercisable on any date up to and including the expiration date)</li>
 * <li>European (exercisable only on the expiration date)</li>
 * <li>Bermudan (exercisable on specific dates – used with swaps and interest rate options)</li>
 * <li>Canary (generally quarterly exercises after one year)</li>
 * <li>Verde (generally yearly exercises after a set holding period)</li>
 * <li>Capped (exercised automatically once a certain level of profit is hit)</li>
 * <li>Compound (option on an option – has two different exercise dates, the first is always European style)</li>
 * <li>Shout (an option that can be provisionally exercised once – the shout – and again at expiration. The holder gets to choose which price they prefer)</li>
 * <li>Swing (a Bermudan with some more additional restrictions used in energy markets)</li>
 * <li>Other (unknown or otherwise unsupported style)</li>
 * </ul>
 */
public enum ExerciseStyle
{
    AMERICAN(1, "A", "American"),
    EUROPEAN(2, "E", "European"),
    BERMUDAN(3, "B", "Bermudan"),
    CANARY(4, "C", "Canary"),
    VERDE(5, "V", "Verde"),
    CAPPED(6, "P", "Capped"),
    COMPOUND(7, "M", "Compound"),
    SHOUT(8, "S", "Shout"),
    SWING(9, "W", "Swing"),
    OTHER(10, "O", "Other");

    private static final Map<Integer, ExerciseStyle> idLookup   = new HashMap<>();
    private static final Map<String, ExerciseStyle>  codeLookup = new HashMap<>();

    static
    {
        for (ExerciseStyle s : EnumSet.allOf(ExerciseStyle.class))
        {
            idLookup.put(s.getId(), s);
            codeLookup.put(s.getCode(), s);
        }
    }

    public static ExerciseStyle of(int id)
    {
        return idLookup.get(id);
    }
    public static ExerciseStyle of(String code)
    {
        return codeLookup.get(code);
    }

    private final int    id;
    private final String code;
    private final String readableName;

    ExerciseStyle(int id, String code, String readableName)
    {
        this.id           = id;
        this.code         = code;
        this.readableName = readableName;
    }

    public Integer getId()
    {
        return id;
    }

    public String getCode()
    {
        return code;
    }

    public String asJsonValue()
    {
        return name();
    }

    public String getReadableName()
    {
        return readableName;
    }

    @Override
    public String toString()
    {
        return name();
    }
}
