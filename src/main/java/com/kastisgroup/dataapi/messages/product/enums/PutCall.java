package com.kastisgroup.dataapi.messages.product.enums;

import com.fasterxml.jackson.annotation.JsonProperty;

public enum PutCall
{
    CALL('C'), PUT('P');

    private char code;

    private PutCall(char code)
    {
        this.code = code;
    }

    @JsonProperty("putCall")
    public char getCode()
    {
        return code;
    }

    public char asJsonValue() {return code;}
}
