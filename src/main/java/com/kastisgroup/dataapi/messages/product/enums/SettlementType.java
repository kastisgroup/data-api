package com.kastisgroup.dataapi.messages.product.enums;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.EnumSet;
import java.util.HashMap;
import java.util.Map;

public enum SettlementType
{
    @JsonProperty("CASH")
    CASH(1, "CASH"),
    @JsonProperty("PHYSICAL")
    PHYSICAL(2, "PHYSICAL"),
    @JsonProperty("BOTH")
    BOTH(3, "BOTH"),
    @JsonProperty("EITHER")
    EITHER(4, "EITHER"),
    @JsonProperty("INVESTOR_CHOICE")
    INVESTOR_CHOICE(5, "INVESTOR-CHOICE"),
    @JsonProperty("ISSUER_CHOICE")
    ISSUER_CHOICE(6, "ISSUER-CHOICE");

    private static final Map<Integer, SettlementType> idLookup   = new HashMap<>();
    private static final Map<String, SettlementType>  jsonLookup = new HashMap<>();

    static {
        for (SettlementType s : EnumSet.allOf(SettlementType.class))
        {
            idLookup.put(s.getId(), s);
            jsonLookup.put(s.asJsonValue(), s);
        }
    }

    public static SettlementType of(int id)      {return idLookup.get(id);}
    public static SettlementType of(String json) {return jsonLookup.get(json);}

    private final int    id;
    private final String readableName;

    SettlementType(int id, String readableName)
    {
        this.id           = id;
        this.readableName = readableName;
    }

    public Integer getId()           {return id;}
    public String  asJsonValue()          {return name();}
    public String  getReadableName() {return readableName;}

    @Override
    public String toString()
    {
        return name();
    }
}
