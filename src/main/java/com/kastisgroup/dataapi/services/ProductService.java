package com.kastisgroup.dataapi.services;

import com.kastisgroup.dataapi.messages.common.components.CrossReferences;
import com.kastisgroup.dataapi.messages.common.components.Symbol;
import com.kastisgroup.dataapi.messages.common.components.Venue;
import com.kastisgroup.dataapi.messages.product.EquityProductInstrument;
import com.kastisgroup.dataapi.messages.product.FutureProductInstrument;
import com.kastisgroup.dataapi.messages.product.OptionProductInstrument;

import java.util.List;
import java.util.Optional;

public class ProductService
{
    public FutureProductInstrument getFutureBySymbol(String symbolProvider, String symbol, String venue)
    {
        return null;
    }

    public EquityProductInstrument getEquityBySymbol(String symbolProvider, String symbol, String venue)
    {
        return null;
    }

    public OptionProductInstrument getOptionBySymbol(String symbolProvider, String symbol, String venue)
    {
        return null;
    }

    public List<Venue> getVenues(Optional<String> venueMIC, Optional<String> venueAcronym)
    {
        return null;
    }

    public List<CrossReferences> getCrossReferences(List<Symbol> lookups)
    {
        return null;
    }
}
