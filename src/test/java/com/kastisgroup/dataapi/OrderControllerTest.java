package com.kastisgroup.dataapi;

import com.kastisgroup.dataapi.controllers.OrderController;
import com.kastisgroup.dataapi.exceptions.OrderNotFoundException;
import com.kastisgroup.dataapi.messages.common.components.Symbol;
import com.kastisgroup.dataapi.messages.order.Order;
import com.kastisgroup.dataapi.messages.order.PriceType;
import com.kastisgroup.dataapi.messages.order.enums.OrderState;
import com.kastisgroup.dataapi.messages.order.enums.Side;
import com.kastisgroup.dataapi.services.OrderService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.math.BigDecimal;

import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.BDDMockito.given;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@WebMvcTest(OrderController.class)
public class OrderControllerTest
{
    // This will create a mock MVC that uses the ProductController (above)
    @Autowired
    private MockMvc mockMvc;

    // This mocks a service that backs the ProductController
    @MockBean
    private OrderService orderService;

    @Test
    public void getOrderByClientOrderId_should_return_order() throws Exception
    {
        // Create a simple order
        Order order = Order.Builder.anOrder()
                                   .withClientOrderId("clOrd1")
                                   .withSystemOrderId("sysOrd2")
                                   .withAccount("ACCT992")
                                   .withSide(Side.BUY)
                                   .withQuantity(BigDecimal.valueOf(100))
                                   .withSymbol(Symbol.of("ISRA", "AAPL"))
                                   .withPriceType(PriceType.LIMIT)
                                   .withPrice(BigDecimal.valueOf(100))
                                   .withFilledQty(BigDecimal.valueOf(20))
                                   .withOrderState(OrderState.WORKING)
                                   .build();

        // Mock up a product service that will return this instrument
        given(orderService.getOrderByClientOrderId(anyString())).willReturn(order);

        // Test the controller - create a query that should hit the above service and call the service - using null venue as a check that it's handled
        TestConfirm.confirmOrder(mockMvc.perform(MockMvcRequestBuilders.get(TestUtils.getOrderByClientOrderIdQuery("clOrd1"))), order);
    }

    @Test
    public void getOrderByClientOrderId_not_found_returns_404() throws Exception
    {
        // Mock up a product service that will throw a specific exception when called
        given(orderService.getOrderByClientOrderId(anyString())).willThrow(new OrderNotFoundException());

        // The controller should make that call and get back a 404
        mockMvc.perform(MockMvcRequestBuilders.get(TestUtils.getOrderByClientOrderIdQuery("clOrd1")))
               .andExpect(status().isNotFound());
    }

    @Test
    public void getOrderBySystemOrderId_should_return_order() throws Exception
    {
        // Create a simple order
        var order = Order.Builder.anOrder()
                                   .withClientOrderId("clOrd1")
                                   .withSystemOrderId("sysOrd2")
                                   .withAccount("ACCT992")
                                   .withSide(Side.BUY)
                                   .withQuantity(BigDecimal.valueOf(100))
                                   .withSymbol(Symbol.of("ISRA", "AAPL"))
                                   .withPriceType(PriceType.LIMIT)
                                   .withPrice(BigDecimal.valueOf(100))
                                   .withOrderState(OrderState.CREATED)
                                   .build();

        // Mock up a product service that will return this instrument
        given(orderService.getOrderBySystemOrderId(anyString())).willReturn(order);

        // Test the controller - create a query that should hit the above service and call the service - using null venue as a check that it's handled
        TestConfirm.confirmOrder(mockMvc.perform(MockMvcRequestBuilders.get(TestUtils.getOrderBySystemOrderIdQuery("clOrd1"))), order);
    }

    @Test
    public void getOrderBySystemOrderId_not_found_returns_404() throws Exception
    {
        // Mock up a product service that will throw a specific exception when called
        given(orderService.getOrderBySystemOrderId(anyString())).willThrow(new OrderNotFoundException());

        // The controller should make that call and get back a 404
        mockMvc.perform(MockMvcRequestBuilders.get(TestUtils.getOrderBySystemOrderIdQuery("clOrd1")))
               .andExpect(status().isNotFound());
    }
}
