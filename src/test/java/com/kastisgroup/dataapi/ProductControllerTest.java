package com.kastisgroup.dataapi;

import com.kastisgroup.dataapi.controllers.ProductController;
import com.kastisgroup.dataapi.exceptions.InstrumentNotFoundException;
import com.kastisgroup.dataapi.messages.common.components.CrossReferences;
import com.kastisgroup.dataapi.messages.common.components.MIC;
import com.kastisgroup.dataapi.messages.common.components.Symbol;
import com.kastisgroup.dataapi.messages.common.components.Venue;
import com.kastisgroup.dataapi.messages.product.EquityProductInstrument;
import com.kastisgroup.dataapi.messages.product.FutureProductInstrument;
import com.kastisgroup.dataapi.messages.product.OptionProductInstrument;
import com.kastisgroup.dataapi.messages.product.enums.PutCall;
import com.kastisgroup.dataapi.services.ProductService;
import com.neovisionaries.i18n.CurrencyCode;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.Month;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.BDDMockito.given;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@WebMvcTest(ProductController.class)
public class ProductControllerTest
{
    // This will create a mock MVC that uses the ProductController (above)
    @Autowired
    private MockMvc mockMvc;

    // This mocks a service that backs the ProductController
    @MockBean
    private ProductService productService;

    // These are some pre-built venues to use in the tests
    private Venue CME  = Venue.Builder.aVenue().withMic(MIC.of("XCME", "XCME")).withAcronym("CME").withFullName("Chicago Mercantile Exchange").build();
    private Venue CBOT = Venue.Builder.aVenue().withMic(MIC.of("XCBT", "XCME")).withAcronym("CBOT").withFullName("Chicago Board of Trade").build();
    private Venue NGS  = Venue.Builder.aVenue().withMic(MIC.of("XNGS", "XNAS")).withAcronym("NGS").withFullName("NASDAQ Global Select").build();

    private Symbol AAPL = Symbol.of("ISRA", "AAPL");
    private Symbol GOOG = Symbol.of("ISRA", "GOOG");
    private Symbol IBM  = Symbol.of("ISRA", "IBM");

    private List<Symbol> ALL_AAPL = Arrays.asList(AAPL, Symbol.of("BLOOMBERG", "AAPL US"), Symbol.of("RIC", "AAPL.N"));
    private List<Symbol> ALL_GOOG = Arrays.asList(GOOG, Symbol.of("BLOOMBERG", "GOOG US"), Symbol.of("RIC", "GOOG.N"));
    private List<Symbol> ALL_IBM  = Arrays.asList(IBM,  Symbol.of("BLOOMBERG", "IBM US"),  Symbol.of("RIC", "IBM.N"));

    private CrossReferences CR_AAPL = CrossReferences.of(AAPL, ALL_AAPL);
    private CrossReferences CR_GOOG = CrossReferences.of(GOOG, ALL_GOOG);
    private CrossReferences CR_IBM  = CrossReferences.of(IBM,  ALL_IBM);

    @Test
    public void getEquityBySymbol_should_return_equity_with_two_venues() throws Exception
    {
        // Create an AAPL equity
        EquityProductInstrument epi = EquityProductInstrument.Builder.anEquityProductInstrument()
                                                                     .withProduct(TestUtils.createEquityProduct(1))
                                                                     .withInstrument(TestUtils.createEquityInstrument(100, CurrencyCode.USD))
                                                                     .withVenues(Arrays.asList(CME, CBOT))
                                                                     .build();

        // Mock up a product service that will return this instrument
        given(productService.getEquityBySymbol(anyString(), anyString(), any())).willReturn(epi);

        // Test the controller - create a query that should hit the above service and call the service - using null venue as a check that it's handled
        TestConfirm.confirmEquity(mockMvc.perform(MockMvcRequestBuilders.get(TestUtils.getEquityBySymbolQuery("Bloomberg", "AAPL.US", null))), epi);
    }

    @Test
    public void getEquityBySymbol_not_found_returns_404() throws Exception
    {
        // Mock up a product service that will throw a specific exception when called
        given(productService.getEquityBySymbol(anyString(), anyString(), any())).willThrow(new InstrumentNotFoundException());

        // The controller should make that call and get back a 404
        mockMvc.perform(MockMvcRequestBuilders.get(TestUtils.getEquityBySymbolQuery("Bloomberg", "AAPL.US", null)))
               .andExpect(status().isNotFound());
    }

    @Test
    public void getFutureBySymbol_should_return_future() throws Exception
    {
        // Create a March 2023 e-Mini S&P future (XXX product level not done yet!)
        FutureProductInstrument fpi = TestUtils.createFutureProductInstrument(1, 100, LocalDate.of(2023, Month.MARCH, 15));

        // Mock up a product service that will return this instrument
        given(productService.getFutureBySymbol(anyString(), anyString(), anyString())).willReturn(fpi);

        // Test the controller - create a query that should hit the above service and call the service
        TestConfirm.confirmFuture(mockMvc.perform(MockMvcRequestBuilders.get(TestUtils.getFutureBySymbolQuery("Bloomberg", "ESH3", "CME"))), fpi);
    }

    @Test
    public void getFutureBySymbol_not_found_returns_404() throws Exception
    {
        // Mock up a product service that will throw a specific exception when called
        given(productService.getFutureBySymbol(anyString(), anyString(), anyString())).willThrow(new InstrumentNotFoundException());

        // The controller should make that call and get back a 404
        mockMvc.perform(MockMvcRequestBuilders.get(TestUtils.getFutureBySymbolQuery("Bloomberg", "ESH3", "CME")))
               .andExpect(status().isNotFound());
    }

    @Test
    public void getOptionBySymbol_should_return_option() throws Exception
    {
        // Create a March 21 2023 250 CALL AAPL option (XXX product level not done yet!)
        OptionProductInstrument opi = TestUtils.createOptionProductInstrument(1, 100, PutCall.CALL, new BigDecimal("850"),
                                                                              LocalDate.of(2023, Month.MARCH, 21));

        // Mock up a product service that will return this instrument
        given(productService.getOptionBySymbol(anyString(), anyString(), anyString())).willReturn(opi);

        // Test the controller - create a query that should hit the above service and call the service
        TestConfirm.confirmOption(mockMvc.perform(MockMvcRequestBuilders.get(TestUtils.getOptionBySymbolQuery("Bloomberg", "AAPL15MAR23250C", "XCBO"))),
                                  opi);
    }

    @Test
    public void getOptionBySymbol_not_found_returns_404() throws Exception
    {
        // Mock up a product service that will throw a specific exception when called
        given(productService.getOptionBySymbol(anyString(), anyString(), anyString())).willThrow(new InstrumentNotFoundException());

        // The controller should make that call and get back a 404
        mockMvc.perform(MockMvcRequestBuilders.get(TestUtils.getOptionBySymbolQuery("Bloomberg", "AAPL15MAR23250C", "XCBO")))
               .andExpect(status().isNotFound());
    }

    @Test
    public void get_full_venue_list() throws Exception
    {
        // Create a list of venues
        List<Venue> venues = Arrays.asList(CME, CBOT, NGS);

        // Mock up a product service that will return this instrument
        given(productService.getVenues(any(), any())).willReturn(venues);

        // Test the controller - create a query that should hit the above service and call the service
        TestConfirm.confirmVenues(mockMvc.perform(MockMvcRequestBuilders.get(TestUtils.getVenuesQuery(Optional.empty(), Optional.empty()))), venues);
    }

    @Test
    public void get_a_venue_by_mic() throws Exception
    {
        // Create a list of venues
        List<Venue> venues = Collections.singletonList(CME);

        // Mock up a product service that will return this instrument
        given(productService.getVenues(any(), any())).willReturn(venues);

        // Test the controller - create a query that should hit the above service and call the service
        TestConfirm.confirmVenues(mockMvc.perform(MockMvcRequestBuilders.get(TestUtils.getVenuesQuery(Optional.of("XCME"), Optional.empty()))), venues);
    }

    @Test
    public void get_a_venue_by_acronym() throws Exception
    {
        // Create a list of venues
        List<Venue> venues = Collections.singletonList(CME);

        // Mock up a product service that will return this instrument
        given(productService.getVenues(any(), any())).willReturn(venues);

        // Test the controller - create a query that should hit the above service and call the service
        TestConfirm.confirmVenues(mockMvc.perform(MockMvcRequestBuilders.get(TestUtils.getVenuesQuery(Optional.empty(), Optional.of("CME")))), venues);
    }

    @Test
    public void get_cross_refs_by_list() throws Exception
    {
        // Create a list of symbol lookups
        List<Symbol> lookups = Arrays.asList(AAPL, GOOG, IBM);

        List<CrossReferences> xrefs = Arrays.asList(CR_AAPL, CR_GOOG, CR_IBM);
        
        // Mock up a product service that will return this instrument
        given(productService.getCrossReferences(any())).willReturn(xrefs);

        // Test the controller - create a query that should hit the above service and call the service
        TestConfirm.confirmCrossReferences(mockMvc.perform(MockMvcRequestBuilders.get(TestUtils.getCrossReferencesQuery(AAPL, GOOG, IBM))), xrefs);
    }
}
