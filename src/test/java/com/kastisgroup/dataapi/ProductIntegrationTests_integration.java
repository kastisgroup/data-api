package com.kastisgroup.dataapi;

import com.kastisgroup.dataapi.controllers.Endpoints;
import com.kastisgroup.dataapi.messages.product.components.CFI;
import com.kastisgroup.dataapi.messages.product.FutureInstrument;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.util.UriComponentsBuilder;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.Month;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.boot.test.context.SpringBootTest.WebEnvironment.RANDOM_PORT;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = RANDOM_PORT)
public class ProductIntegrationTests_integration {

    @Autowired
    private TestRestTemplate restTemplate;

    @Test
    public void test_futures_lookup()
    {
        String uri = UriComponentsBuilder.fromPath(Endpoints.GET_FUTURE_BY_SYMBOL)
                                         .queryParam("SymbolProvider", "Bloomberg")
                                         .queryParam("Symbol", "ESH3")
                                         .queryParam("Venue", "XCME")
                                         .toUriString();

        FutureInstrument fi = FutureInstrument.Builder.aFutureInstrument()
                                                      .withId(1)
                                                      .withFirstTradingDate(LocalDate.of(2022, Month.MARCH, 1))
                                                      .withLastTradingDate(LocalDate.of(2023, Month.MARCH, 15))
                                                      .withCFI(CFI.of("FXXXXX"))
                                                      .withContractSize(new BigDecimal("5000.0"))
                                                      .withMultiplier(new BigDecimal("100.0"))
                                                      .withContractDate(LocalDate.of(2023, Month.MARCH, 15))
                                                      .withValuationDate(null)
                                                      .withRate(null).build();

        ResponseEntity<FutureInstrument> response = restTemplate.getForEntity(uri, FutureInstrument.class);

        assertThat(response.getStatusCode()).isEqualTo((HttpStatus.OK));
        assertThat(response.getBody()).isEqualTo(fi);

    }
}
