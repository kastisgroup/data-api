package com.kastisgroup.dataapi;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.kastisgroup.dataapi.messages.common.components.CrossReferences;
import com.kastisgroup.dataapi.messages.common.components.Venue;
import com.kastisgroup.dataapi.messages.order.Order;
import com.kastisgroup.dataapi.messages.product.*;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.web.servlet.ResultActions;

import java.util.List;
import java.util.stream.Collectors;

import static org.hamcrest.CoreMatchers.hasItems;
import static org.hamcrest.Matchers.hasSize;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

public class TestConfirm
{

    public static ResultActions confirmOK(ResultActions resultActions) throws Exception
    {
        return resultActions.andExpect(status().isOk());
    }

    public static ResultActions confirmNotFound(ResultActions resultActions) throws Exception
    {
        return resultActions.andExpect(status().isNotFound());
    }

    private static String safeToString(Object o)
    {
        if (o == null)
            return null;
        else
            return o.toString();
    }

    public static ResultActions confirmEquity(ResultActions resultActions, EquityProductInstrument epi) throws Exception
    {
        EquityProduct    ep = epi.getProduct();
        EquityInstrument ei = epi.getInstrument();

        checkVenues(resultActions, epi.getVenues(), "venues.[*]");

        return resultActions.andExpect(status().isOk())
                            .andExpect(jsonPath("product.id").value(ep.getId()))
                            .andExpect(jsonPath("product.assetClass").value(ep.getAssetClass().asJsonValue()))
                            .andExpect(jsonPath("product.assetCategory").value(ep.getAssetCategory().asJsonValue()))
                            .andExpect(jsonPath("product.currency").value(ep.getCurrencyCode()))
                            .andExpect(jsonPath("product.contractSize").value(ep.getContractSize()))
                            .andExpect(jsonPath("product.lotSize").value(ep.getLotSize()))
                            .andExpect(jsonPath("product.multiplier").value(ep.getMultiplier()))
                            .andExpect(jsonPath("product.tickSize").value(ep.getTickSize()))
                            .andExpect(jsonPath("product.countryOfIssue").value(ep.getCountryOfIssueAlpha3()))
                            .andExpect(jsonPath("instrument.id").value(ei.getId()))
                            .andExpect(jsonPath("instrument.firstTradingDate").value(safeToString(ei.getFirstTradingDate())))
                            .andExpect(jsonPath("instrument.lastTradingDate").value(safeToString(ei.getLastTradingDate())))
                            .andExpect(jsonPath("instrument.cfiCode").value(safeToString(ei.getCFI())))
                            .andExpect(jsonPath("instrument.contractSize").value(safeToString(ei.getContractSize())))
                            .andExpect(jsonPath("instrument.multiplier").value(safeToString(ei.getMultiplier())))
                            .andExpect(jsonPath("instrument.tradingCurrency").value(safeToString(ei.getTradingCurrency())))
                            .andExpect(jsonPath("instrument.quotationCurrency").value(safeToString(ei.getQuotationCurrency())));
    }

    public static ResultActions confirmFuture(ResultActions resultActions, FutureProductInstrument fpi) throws Exception
    {
        FutureProduct    fp = fpi.getProduct();
        FutureInstrument fi = fpi.getInstrument();

        return resultActions.andExpect(status().isOk())
                            .andExpect(jsonPath("product.id").value(fp.getId()))
                            .andExpect(jsonPath("product.assetClass").value(fp.getAssetClass().asJsonValue()))
                            .andExpect(jsonPath("product.assetCategory").value(fp.getAssetCategory().asJsonValue()))
                            .andExpect(jsonPath("product.currency").value(fp.getCurrencyCode()))
                            .andExpect(jsonPath("product.contractSize").value(fp.getContractSize()))
                            .andExpect(jsonPath("product.lotSize").value(fp.getLotSize()))
                            .andExpect(jsonPath("product.multiplier").value(fp.getMultiplier()))
                            .andExpect(jsonPath("product.tickSize").value(fp.getTickSize()))
                            .andExpect(jsonPath("product.derivativeType").value(fp.getDerivativeType().asJsonValue()))
                            .andExpect(jsonPath("product.settlementType").value(fp.getSettlementType().asJsonValue()))
                            .andExpect(jsonPath("instrument.id").value(fi.getId()))
                            .andExpect(jsonPath("instrument.firstTradingDate").value(safeToString(fi.getFirstTradingDate())))
                            .andExpect(jsonPath("instrument.lastTradingDate").value(safeToString(fi.getLastTradingDate())))
                            .andExpect(jsonPath("instrument.cfiCode").value(safeToString(fi.getCFI())))
                            .andExpect(jsonPath("instrument.contractSize").value(safeToString(fi.getContractSize())))
                            .andExpect(jsonPath("instrument.multiplier").value(safeToString(fi.getMultiplier())))
                            .andExpect(jsonPath("instrument.contractDate").value(safeToString(fi.getContractDate())))
                            .andExpect(jsonPath("instrument.valuationDate").value(safeToString(fi.getValuationDate())))
                            .andExpect(jsonPath("instrument.rate").value(safeToString(fi.getRate())));
    }

    public static ResultActions confirmOption(ResultActions resultActions, OptionProductInstrument opi) throws Exception
    {
        OptionProduct    op = opi.getProduct();
        OptionInstrument oi = opi.getInstrument();

        return resultActions.andExpect(status().isOk())
                            .andExpect(jsonPath("product.id").value(op.getId()))
                            .andExpect(jsonPath("product.assetClass").value(op.getAssetClass().asJsonValue()))
                            .andExpect(jsonPath("product.assetCategory").value(op.getAssetCategory().asJsonValue()))
                            .andExpect(jsonPath("product.currency").value(op.getCurrencyCode()))
                            .andExpect(jsonPath("product.contractSize").value(op.getContractSize()))
                            .andExpect(jsonPath("product.lotSize").value(op.getLotSize()))
                            .andExpect(jsonPath("product.multiplier").value(op.getMultiplier()))
                            .andExpect(jsonPath("product.tickSize").value(op.getTickSize()))
                            .andExpect(jsonPath("product.derivativeType").value(op.getDerivativeType().asJsonValue()))
                            .andExpect(jsonPath("product.settlementType").value(op.getSettlementType().asJsonValue()))
                            .andExpect(jsonPath("product.strikeCurrency").value(op.getStrikeCurrencyCode()))
                            .andExpect(jsonPath("product.strikePriceMultipler").value(op.getStrikePriceMultipler()))
                            .andExpect(jsonPath("product.strikePriceShift").value(op.getStrikePriceShift()))
                            .andExpect(jsonPath("product.version").value(op.getVersion()))
                            .andExpect(jsonPath("product.generation").value(op.getGeneration()))
                            .andExpect(jsonPath("instrument.id").value(oi.getId()))
                            .andExpect(jsonPath("instrument.firstTradingDate").value(safeToString(oi.getFirstTradingDate())))
                            .andExpect(jsonPath("instrument.lastTradingDate").value(safeToString(oi.getLastTradingDate())))
                            .andExpect(jsonPath("instrument.cfiCode").value(safeToString(oi.getCFI())))
                            .andExpect(jsonPath("instrument.contractSize").value(safeToString(oi.getContractSize())))
                            .andExpect(jsonPath("instrument.multiplier").value(safeToString(oi.getMultiplier())))
                            .andExpect(jsonPath("instrument.putCall").value(safeToString(oi.getPutCall())))
                            .andExpect(jsonPath("instrument.strike").value(safeToString(oi.getStrike())))
                            .andExpect(jsonPath("instrument.keyDate").value(safeToString(oi.getKeyDate())))
                            .andExpect(jsonPath("instrument.contractDate").value(safeToString(oi.getContractDate())))
                            .andExpect(jsonPath("instrument.expirationDate").value(safeToString(oi.getExpirationDate())));
    }

    public static ResultActions confirmOrder(ResultActions resultActions, Order order) throws Exception
    {
        return resultActions.andDo(print())
                            .andExpect(status().isOk())
                            .andExpect(jsonPath("clientOrderId").value(order.getClientOrderId()))
                            .andExpect(jsonPath("systemOrderId").value(order.getSystemOrderId()))
                            .andExpect(jsonPath("clientReplacementId").value(order.getClientReplacementId()))
                            .andExpect(jsonPath("systemReplacementId").value(order.getSystemReplacementId()))
                            .andExpect(jsonPath("account").value(order.getAccount()))
                            .andExpect(jsonPath("side").value(order.getSide().asJsonValue()))
                            .andExpect(jsonPath("quantity").value(order.getQuantity()))
                            .andExpect(jsonPath("symbolProvider").value(order.getSymbol().getSymbolProvider()))
                            .andExpect(jsonPath("symbol").value(order.getSymbol().getSymbol()))
                            .andExpect(jsonPath("priceType").value(order.getPriceType().asJson()))
                            .andExpect(jsonPath("price").value(order.getPrice()))
                            .andExpect(jsonPath("stopPrice").value(order.getStopPrice()))
                            .andExpect(jsonPath("filledQty").value(order.getFilledQty()))
                            .andExpect(jsonPath("lostQty").value(order.getLostQty()))
                            .andExpect(jsonPath("orderState").value(order.getOrderState().asJson()));
    }

    private static void checkVenues(ResultActions resultActions, List<Venue> venues, String prefix) throws Exception
    {
        if (venues == null || venues.isEmpty())
            return;
        
        Object[] acronyms      = venues.stream().map(Venue::getAcronym).toArray();
        Object[] fullNames     = venues.stream().map(Venue::getFullName).toArray();
        Object[] mics          = venues.stream().map(i -> i.getMic().getMic()).toArray();
        Object[] operatingMICs = venues.stream().map(i -> i.getMic().getOperatingMIC().orElse("")).toArray();

        // Make sure the action is as expected
        resultActions.andExpect(jsonPath(prefix+".acronym",      hasItems(acronyms)))
                     .andExpect(jsonPath(prefix+".fullName",     hasItems(fullNames)))
                     .andExpect(jsonPath(prefix+".mic",          hasItems(mics)))
                     .andExpect(jsonPath(prefix+".operatingMIC", hasItems(operatingMICs)));
    }

    public static void confirmVenues(ResultActions resultActions, List<Venue> venues) throws Exception
    {
        // Make sure the action is as expected
        resultActions.andExpect(status().isOk());

        ObjectMapper mapper = new ObjectMapper();

        List<String> vs = venues.stream().map(v -> v.toJson(mapper)).collect(Collectors.toList());

        MockHttpServletResponse response = resultActions.andReturn().getResponse();

        // Check that a list of venues came back at the root level
        checkVenues(resultActions, venues, "$.[*]");
    }

    private static void checkCrossReferences(ResultActions resultActions, List<CrossReferences> xrefs, String prefix) throws Exception
    {
        if (xrefs == null || xrefs.isEmpty())
            return;

        // Make sure the action is as expected
        resultActions.andExpect(jsonPath(prefix, hasSize(xrefs.size())));

        // XXX I completely give up trying to figure out how to match CrossReferences via jsonPath
        // XXX The resultActions contains JSON that has no quotes but the matcher generates quotes on
        // XXX any JSON objects that you provide
    }

    public static void confirmCrossReferences(ResultActions resultActions, List<CrossReferences> xrefs) throws Exception
    {
        // Make sure the action is as expected
        resultActions.andExpect(status().isOk());

        // Check that a list of cross-references came back at the root level
        checkCrossReferences(resultActions, xrefs, "$.[*]");
    }

}
