package com.kastisgroup.dataapi;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.kastisgroup.dataapi.controllers.Endpoints;
import com.kastisgroup.dataapi.messages.common.components.Symbol;
import com.kastisgroup.dataapi.messages.order.Order;
import com.kastisgroup.dataapi.messages.order.PriceType;
import com.kastisgroup.dataapi.messages.order.enums.Side;
import com.kastisgroup.dataapi.messages.product.*;
import com.kastisgroup.dataapi.messages.product.components.CFI;
import com.kastisgroup.dataapi.messages.product.enums.*;
import com.neovisionaries.i18n.CountryCode;
import com.neovisionaries.i18n.CurrencyCode;
import org.springframework.web.util.UriComponentsBuilder;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.Optional;

public class TestUtils
{
    private static String getBySymbolQuery(String endpoint, String symbolProvider, String symbol, String venue)
    {
        UriComponentsBuilder builder = UriComponentsBuilder.fromPath(endpoint)
                                                           .queryParam("SymbolProvider", symbolProvider)
                                                           .queryParam("Symbol", symbol);

        if (venue != null)
            builder.queryParam("Venue", venue);

        return builder.toUriString();
    }

    private static String getOrderByIdQuery(String clientOrderId, String systemOrderId)
    {
        UriComponentsBuilder builder = UriComponentsBuilder.fromPath(Endpoints.GET_ORDER_BY_ID);

        if (clientOrderId != null)
            builder.queryParam("ClientOrderId", clientOrderId);

        if (systemOrderId != null)
            builder.queryParam("SystemOrderId", systemOrderId);

        return builder.toUriString();
    }

    public static String getVenuesQuery(Optional<String> mic, Optional<String> acronym)
    {
        UriComponentsBuilder builder = UriComponentsBuilder.fromPath(Endpoints.GET_VENUES);

        if (mic.isPresent())
            builder.queryParam("MIC", mic.get());

        if (acronym.isPresent())
            builder.queryParam("Acronym", acronym);

        return builder.toUriString();
    }

    public static String getCrossReferencesQuery(Symbol... symbols)
    {
        UriComponentsBuilder builder = UriComponentsBuilder.fromPath(Endpoints.GET_CROSS_REFERENCES);
        final ObjectMapper mapper = new ObjectMapper();

        builder.queryParam("lookups", symbols); // XXX This uses toString() to convert the symbols, not Json. Using ObjectMapper.writeValueAsString is no better

        return builder.toUriString();
    }

    public static String getEquityBySymbolQuery(String symbolProvider, String symbol, String venue)
    {
        return getBySymbolQuery(Endpoints.GET_EQUITY_BY_SYMBOL, symbolProvider, symbol, venue);
    }

    public static String getFutureBySymbolQuery(String symbolProvider, String symbol, String venue)
    {
        return getBySymbolQuery(Endpoints.GET_FUTURE_BY_SYMBOL, symbolProvider, symbol, venue);
    }

    public static String getOptionBySymbolQuery(String symbolProvider, String symbol, String venue)
    {
        return getBySymbolQuery(Endpoints.GET_OPTION_BY_SYMBOL, symbolProvider, symbol, venue);
    }

    public static String getOrderByClientOrderIdQuery(String clientOrderId)
    {
        return getOrderByIdQuery(clientOrderId, null);
    }

    public static String getOrderBySystemOrderIdQuery(String systemOrderId)
    {
        return getOrderByIdQuery(null, systemOrderId);
    }

    public static EquityProduct createEquityProduct(long id)
    {
        return EquityProduct.Builder.anEquityProduct()
                                    .withId(id)
                                    .withAssetClass(AssetClass.EQUITY)
                                    .withAssetCategory(AssetCategory.ORDINARY_SHARES)
                                    .withCurrency(CurrencyCode.USD)
                                    .withContractSize(BigDecimal.ONE)
                                    .withLotSize(BigDecimal.ONE)
                                    .withMultiplier(BigDecimal.ONE)
                                    .withTickSize(new BigDecimal(".01"))
                                    .withCountryOfIssue(CountryCode.US)
                                    .build();
    }

    public static EquityInstrument createEquityInstrument(long id, CurrencyCode tradingCurrencyCode)
    {
        return EquityInstrument.Builder.anEquityInstrument()
                                       .withId(id)
                                       .withFirstTradingDate(LocalDate.of(1880, 1, 1))
                                       .withLastTradingDate(LocalDate.of(9999, 12, 31))
                                       .withCFI(CFI.of("EXXXXX"))
                                       .withContractSize(new BigDecimal("1.0"))
                                       .withMultiplier(new BigDecimal("1.0"))
                                       .withTradingCurrency(tradingCurrencyCode)
                                       .withQuotationCurrency(tradingCurrencyCode)
                                       .build();
    }

    public static FutureProduct createFutureProduct(long id)
    {
        return FutureProduct.Builder.aFutureProduct()
                                    .withId(id)
                                    .withAssetClass(AssetClass.FUTURE)
                                    .withAssetCategory(AssetCategory.UNITS)
                                    .withCurrency(CurrencyCode.USD)
                                    .withContractSize(new BigDecimal("100"))
                                    .withLotSize(new BigDecimal("5000"))
                                    .withMultiplier(new BigDecimal("100"))
                                    .withTickSize(new BigDecimal(".1"))
                                    .withDerivativeType(DerivativeType.STANDARD)
                                    .withSettlementType(SettlementType.CASH)
                                    .build();
    }

    public static FutureInstrument createFutureInstrument(long id, LocalDate lastTradingDate)
    {
        return FutureInstrument.Builder.aFutureInstrument()
                                       .withId(id)
                                       .withFirstTradingDate(lastTradingDate.minusYears(1))
                                       .withLastTradingDate(lastTradingDate)
                                       .withCFI(CFI.of("FXXXXX"))
                                       .withContractSize(new BigDecimal("5000.0"))
                                       .withMultiplier(new BigDecimal("100.0"))
                                       .withContractDate(lastTradingDate)
                                       .withValuationDate(null)
                                       .withRate(null).build();
    }

    public static EquityProductInstrument createEquityProductInstrument(long productId, long instrumentId,
                                                                        CurrencyCode tradingCurrencyCode)
    {
        return EquityProductInstrument.Builder.anEquityProductInstrument()
                                              .withProduct(createEquityProduct(productId))
                                              .withInstrument(createEquityInstrument(instrumentId, tradingCurrencyCode))
                                              .build();
    }


    public static OptionProductInstrument createOptionProductInstrument(long productId, long instrumentId,
                                                                        PutCall putCall, BigDecimal strike,
                                                                        LocalDate keyDate)
    {
        return OptionProductInstrument.Builder.anOptionProductInstrument()
                                              .withProduct(createOptionProduct(productId))
                                              .withInstrument(
                                                      createOptionInstrument(instrumentId, putCall, strike, keyDate))
                                              .build();
    }

    public static OptionProduct createOptionProduct(long id)
    {
        return OptionProduct.Builder.anOptionProduct()
                                    .withId(id)
                                    .withAssetClass(AssetClass.OPTION)
                                    .withAssetCategory(AssetCategory.ORDINARY_SHARES)
                                    .withCurrency(CurrencyCode.USD)
                                    .withContractSize(new BigDecimal("100"))
                                    .withLotSize(new BigDecimal("100"))
                                    .withMultiplier(new BigDecimal("100"))
                                    .withTickSize(new BigDecimal(".01"))
                                    .withExerciseStyle(ExerciseStyle.AMERICAN)
                                    .withDerivativeType(DerivativeType.STANDARD)
                                    .withSettlementType(SettlementType.CASH)
                                    .withStrikeCurrency(CurrencyCode.USD)
                                    .withStrikePriceMultipler(new BigDecimal("100"))
                                    .withStrikePriceShift(BigDecimal.TEN)
                                    .withVersion(1)
                                    .withGeneration(1)
                                    .build();
    }

    public static OptionInstrument createOptionInstrument(long id, PutCall putCall, BigDecimal strike,
                                                          LocalDate keyDate)
    {
        return OptionInstrument.Builder.anOptionInstrument()
                                       .withId(id)
                                       .withFirstTradingDate(keyDate.minusYears(1))
                                       .withLastTradingDate(keyDate)
                                       .withCFI(CFI.of("OXXXXX"))
                                       .withContractSize(new BigDecimal("100.0"))
                                       .withMultiplier(new BigDecimal("99.7"))
                                       .withPutCall(putCall)
                                       .withStrike(strike)
                                       .withKeyDate(keyDate)
                                       .withExpirationDate(keyDate).withContractDate(keyDate).build();

    }

    public static FutureProductInstrument createFutureProductInstrument(int productId, int instrumentId,
                                                                        LocalDate lastTradingDate)
    {
        return FutureProductInstrument.Builder.anFutureProductInstrument()
                                              .withProduct(createFutureProduct(productId))
                                              .withInstrument(createFutureInstrument(instrumentId, lastTradingDate))
                                              .build();
    }

    public static Order createOrder(String clientOrderId, String systemOrderId,
                                    String account, Side side,
                                    BigDecimal quantity,
                                    PriceType priceType, BigDecimal price,
                                    BigDecimal filledQty)
    {
        return Order.Builder.anOrder()
                            .withAccount(account)
                            .withClientOrderId(clientOrderId)
                            .withClientReplacementId(null)
                            .withFilledQty(filledQty)
                            .withLostQty(BigDecimal.ZERO)
                            .withPrice(price)
                            .withPriceType(priceType)
                            .withQuantity(quantity)
                            .withSide(side)
                            .withStopPrice(null)
                            .withSymbol(Symbol.of("ISRA", "AAPL"))
                            .withSystemOrderId(systemOrderId)
                            .withSystemReplacementId(null).build();
    }

}
